let trainer = {
    name: "Ash Ketchum",
    age: 10,
    
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(pokemon) {
        console.log(pokemon + "! I choose you!");
    }
}
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

console.log("Result of talk method:");
trainer.talk("Pikachu");

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));

        target.health = target.health - this.attack;
        
        if(target.health <= 0) {
            console.log(target.name + " fainted");
        }
    };
    this.faint = function(target) {
        console.log(target.name + " fainted");
    }
}

let Pikachu = new Pokemon("Pikachu", 12);
console.log(Pikachu);

let Geodude = new Pokemon("Geodude", 8);
console.log(Geodude);

let Mewtwo = new Pokemon("Mewtwo", 100);
console.log(Mewtwo);

Geodude.tackle(Pikachu);
console.log(Pikachu);

Mewtwo.tackle(Geodude);
console.log(Geodude);